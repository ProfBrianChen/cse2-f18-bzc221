//Bryce Cavey
//CSE 2
// 11/08/18
//Lab 8

public class lab08{
  public static void main(String args[]){
    int numbers[] = new int[100];
    int numTimes[] = new int[100];
    
    //assign random numbers to numbers
    for (int i = 0; i < 100; i++){
      numbers[i] = (int)(Math.random()*99);
    }
    
    //Do the actual counting with nested for loop and then print out matches
    for (int i = 0; i < 100; i++){
      int count = 0;
      for(int j = 0; j < 100; j++){
        if(numbers[j] == i){
          count++;
        }
      }
      numTimes[i] = count;
      System.out.println(i + " occurs " + numTimes[i] + " times.");
    }
  }
}