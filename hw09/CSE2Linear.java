//Bryce Cavey
//CSE 2
// 11/20/18
//HW 9

import java.util.Scanner;

public class CSE2Linear{
  
 
  public static void binSearch(int search, int[] grades){
    int mid;
    int low =0;
    int high;
    int i = 0;
    //assign high to the max length initially
    high = grades.length - 1;

    while (high >= low) {
       i = i+1;
       mid = (high + low) / 2;
       if (grades[mid] < search) {
          low = mid + 1;
       } 
       else if (grades[mid] > search) {
          high = mid - 1;
       } 
       else {
          System.out.println("Found the value in " + i + " iterations!");
          System.out.println(" ");
          return;
       }
    }
    System.out.println("Did not find the value in " + i + " iterations.");
    System.out.println(" ");
    return;
  }
  
  public static void linSearch(int search, int[] grades){
    int i = 0;
    while (i < grades.length) {
       if(grades[i] == search){
         System.out.println("Found the value in " + (i+1) + " iterations!");
         System.out.println(" ");
         return;
       }
       i = i+1;
    }
    System.out.println("Did not find the value in " + i + " iterations.");
    System.out.println(" ");
    return;
  }
  
    public static int[] scramble(int[] grades){
    int index = 1;
    int tempVar = 0;
    for(int k = 0; k<150; k++){
      //generate random number
      index = (int)(Math.random()*grades.length);
      
      //use vars and temp vars to swap values
      tempVar = grades[index];
        
      grades[index] = grades[0];
      grades[0] = tempVar;
    }
    return grades;
  }
  
  public static void main(String args[]){
    //Prompt user and set up a scanner and array
    System.out.println("Please enter 15 integers for the students final grades, from smallest to largest.");
    Scanner myScanner = new Scanner(System.in);
    
    int[] grades = new int[15]; 
    int search = -1;
    
    
    //Error checking the input
    for(int n = 0; n <15; n++) {
      //checks if it is the proper type
        while (myScanner.hasNext()) {
          if (myScanner.hasNextInt()){
            int temp = myScanner.nextInt();
            if(temp > -1 && temp < 101){
                if(n == 0){
                  grades[n] = temp;
                  break;
                }
                else{
                  if(temp >= grades[n-1]){
                    grades[n] = temp;
                    break;
                  }
                  else{
                    System.out.println("Error please enter an integer greater than  or equal to the last one entered");
                    //myScanner.next();
                  }
                }
            }
            else{
              System.out.println("Error please enter an integer between 0-100");
            }
          }
          //prints error and allows repeat of loop
          else {
            System.out.println("Error please enter an integer");
            myScanner.next();
          } 
      }
    }
    
    //Print the final input array
    for(int i = 0; i < grades.length; i++){
      System.out.print(grades[i] + " ");
    }
    System.out.println("");
    
    //promptu ser and assigns it to search
    System.out.println("Enter a grade to be searched for");
    search = myScanner.nextInt();
    
    //do the binary search and output results
    binSearch(search, grades);
    
    //scramble it all up quick
    System.out.println("Scrambling the array");
    System.out.println(" ");
    grades = scramble(grades);
    for(int i = 0; i < grades.length; i++){
      System.out.print(grades[i] + " ");
    }
    System.out.println("");
    System.out.println("Enter a value to search for linearly.");
    
    search = myScanner.nextInt();
   
    linSearch(search, grades);
    
  }
}
  