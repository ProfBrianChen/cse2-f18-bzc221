//Bryce Cavey
//CSE 2
// 11/20/18
//HW 9

import java.util.Scanner;

//program that was given to me
public class RemoveElements{
  public static void main(String [] arg){
    Scanner scan=new Scanner(System.in);
    int num[]=new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
    String answer="";
    do{
      System.out.print("Random input 10 ints [0-9]");
      num = randomInput();
      String out = "The original array is:";
      out += listArray(num);
      System.out.println(out);

      System.out.print("Enter the index ");
      index = scan.nextInt();
      newArray1 = delete(num,index);
      String out1="The output array is ";
      out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);

        System.out.print("Enter the target value ");
      target = scan.nextInt();
      newArray2 = remove(newArray1,target);
      String out2="The output array is ";
      out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
      System.out.println(out2);

      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
      answer=scan.next();
    }
    while(answer.equals("Y") || answer.equals("y"));
  }

  public static String listArray(int num[]){
    String out="{";
    for(int j=0;j<num.length;j++){
      if(j>0){
        out+=", ";
      }
      out+=num[j];
    }
    out+="} ";
    return out;
  }
  
  public static int[] randomInput(){
    int[] array = new int[10];
    for (int i = 0; i < 10; i++){
      array[i] = (int)(Math.random()*9);
    }
    return array;
  }
  
  //method to remove a spot
  public static int[] delete(int num[], int pos){
    //define a new array, but one shorter
    int[] newArray = new int[num.length-1];
    int index = 0;
    if (pos < 0 || pos > 9){
      System.out.println("Index is not valid");
      return num;
    }
    for (int i = 0; i < num.length; i++){
      if ( i == pos){
        //dont do anything this time
      }
      else{
        newArray[index] = num[i];
        index++;
      }
    }
    return newArray;
  }
  
  
  //Method to remove a value
  public static int[] remove(int num[], int target){
    //Check to see how many total matches
    int matches = 0;
    for (int j = 0; j < num.length; j++){
      if (num[j] == target){
        matches++;
      }
    }
    //Print whether it was found or not
    if (matches == 0){
      System.out.println("Element " + target + " was not found.");
      return num;
    }
    else{
      System.out.println("Element " + target + " has been found with " + matches + " matches.");
    }
    
    int[] newArray = new int[num.length-matches];
    int index = 0;
    //do the replacey
    for (int i = 0; i < num.length; i++){
      if (num[i] == target){
        //dont do anything this time
      }
      else{
        newArray[index] = num[i];
        index++;
      }
    }
    return newArray;
  }
  
}
