//Bryce Cavey
//CSE 2
// 12/1/18
//HW 10

import java.util.Scanner;

public class HW10{
  
  ////////
  //Main//
  ////////
  public static void main(String args[]){
    
    //Set up scanner
    Scanner myScan = new Scanner(System.in);
    
    //Set up array
    String[][] board = {  {"1","2","3"}, 
                          {"4","5","6"}, 
                          {"7","8","9"} };
 
    //Set up player and a check variable for winning
    int win = 0;
    int player = 1;
    String sign = "O";
    int turns = 0;
    
    //Runn consistenyl until a winner is found
    while (win == 0){
      //Initially print out the board
      printBoard(board);
      //If the board is full and its a tie, let them know
      if (turns == 9){
        System.out.println("The game ended in a tie!");
        break;
      }
      //Prompt user for first input
      System.out.println("Player " + player + ", enter a number corresponding to where you would like to mark." );
       
      
      //All the input error checking, move recording, and win check
       while (myScan.hasNext()) {
         //checks if it is twithin range
         if (myScan.hasNextInt()){
           int g = myScan.nextInt();
           if((g < 10 && g > 0) && (boardCheck(board, g) == 1)){
            System.out.println("Move recorded");
            int x = (g-1)%3;
            int y = (g-1)/3;
            board[y][x] = sign;
            turns++;
            break;
           }
           else if(g > 9 || g < 1){
             System.out.println("Error please enter an integer on the board between 1 and 9");
           }
           else{
             System.out.println("Error, this place on the board has already been selected, please select another.");
           }
         }
         //prints error and allows repeat of loop
         else {
           System.out.println("Error please enter an integer on the board between 1 and 9");
           myScan.next();
         }
       }
      if (winCheck(board) == 1){
        printBoard(board);
        System.out.println("Winner is player " + player + "!");
        break;
      }
      else{
        //Switcharoo boi
        if (sign == "O"){ sign = "X";} else{ sign = "O";}
        if (player == 2){ player = 1;} else{ player = 2;}
      }
        
    }
  }
   
  ///////////////
  //Print Board//
  ///////////////
  public static void printBoard(String[][] board){
    System.out.println("");
      for (int i = 0; i < 3; i++){
        for (int j = 0; j < 3; j++){
          System.out.print(board[i][j] + " \t ");
        }
          System.out.println(" ");
      }
    System.out.println("");
  }
  
  ///////////////
  //Check Board//
  ///////////////
  public static int boardCheck(String[][] board, int spot){
    int x = (spot-1)%3;
    int y = (spot-1)/3; 
    if ((board[y][x] == "O") || (board[y][x] == "X")){
      return 0;
      //Aka false case
    }
    else {
      return 1;
      //Aka true case
    }
  }
  
  public static int winCheck(String[][] board){
    //Variable if it hits 3 then there is a win
    int countx = 0;
    int counto = 0;
    //Check all columns
    for (int c = 0; c < 3; c++){
      //reset counters
      countx = 0;
      counto = 0;
      for (int l = 0; l < 3; l++){
        if (board[l][c] == "O"){counto++;}
        if (board[l][c] == "X"){countx++;}
      }
      if (countx == 3 || counto == 3){
        return 1;
      }
    }
    
    //reset counters
    countx = 0;
    counto = 0;
    
    //Check all rows
    for (int r = 0; r < 3; r++){
      //reset counters
      countx = 0;
      counto = 0;
      for (int l = 0; l < 3; l++){
        if (board[r][l] == "O"){counto++;}
        if (board[r][l] == "X"){countx++;}
      }
      if (countx == 3 || counto == 3){
        return 1;
      }
    }
    
    //reset counters
    countx = 0;
    counto = 0;
    //Check diagnol 1
    for (int d = 0; d < 3; d++){
      if (board[d][d] == "O"){counto++;}
      if (board[d][d] == "X"){countx++;}
    }
    if (countx == 3 || counto == 3){
        return 1;
    }
    
    //reset counters
    countx = 0;
    counto = 0;
    //Check diagnol 2
    for (int d = 2; d > -1; d--){
      if (board[2-d][d] == "O"){counto++;}
      if (board[2-d][d] == "X"){countx++;}
    }
    if (countx == 3 || counto == 3){
        return 1;
    }
    //default no win
    return 0;
  }
  
  
}
    
    