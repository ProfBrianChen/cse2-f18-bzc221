import java.util.Scanner;
public class Shuffling{
  
  //method to print out all cards
  public static void printArray(String[] list){
    //for loop to cycle through and print the lists out
    for(int i = 0; i < list.length; i++){
      System.out.print(list[i] + " ");
    }
    return;
  }
  //shuffle method
  public static String[] shuffle(String[] list){
    int index = 1;
    String tempVar = "";
    for(int k = 0; k<100; k++){
      //generate random number
      index = (int)(Math.random()*list.length);
      
      //use vars and temp vars to swap values
      tempVar = list[index];
        
      list[index] = list[0];
      list[0] = tempVar;
    }
    return list;
  }
  //Class to generate a hand. 
  public static String[] getHand(String[] list, int index, int numCards){
    String[] hand = new String[numCards];
    //for loop to generate the last 5 cards and make them a hand
    for(int j = 0; j < numCards; j++){
      hand[j] = list[index-j];
    }
    return hand;
  }
  
  
  public static void main(String[] args) { 
    Scanner scan = new Scanner(System.in); 
     //suits club, heart, spade or diamond 
    String[] suitNames={"C","H","S","D"};    
    String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
    String[] cards = new String[52]; 
    String[] hand = new String[5]; 
    int numCards = 5; 
    int again = 1; 
    int index = 51;
    //Generates a new card deck WITHOUT printing
    for (int i=0; i<52; i++){ 
      cards[i]=rankNames[i%13]+suitNames[i/13]; 
    } 
    System.out.println();
    printArray(cards);
    System.out.println();
    System.out.println("Shuffling");
    System.out.println();
    cards = shuffle(cards); 
    printArray(cards); 
    
    //while loop to see if the user wants another hand to be drawn.
    while(again == 1){ 
       //if statement that determines if there are enough cards left in the deck. 
       if (numCards > index){
          System.out.println();
          System.out.println("Out of cards, making a new deck.");
          System.out.println();
          //Generates a new card deck
          for (int i=0; i<52; i++){ 
            cards[i]=rankNames[i%13]+suitNames[i/13]; 
            System.out.print(cards[i]+" "); 
          }
         System.out.println();
         System.out.println("Shuffling");
         System.out.println();
         cards = shuffle(cards);
         printArray(cards);
         index = 51;
       }
      
       //gets a hand and prints it
       hand = getHand(cards,index,numCards);
       System.out.println();
       System.out.println("Hand:");
       System.out.println();
       printArray(hand);
       System.out.println();
       index = index - numCards;
       //checks if user wants another hand
       System.out.println("Enter a 1 if you want another hand drawn"); 
       again = scan.nextInt(); 
    }  
  } 
}
