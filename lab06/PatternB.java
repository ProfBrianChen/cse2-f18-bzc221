//Bryce Cavey
//CSE 2
// 10/10/18
//Lab 6

import java.util.Scanner;

public class PatternB{
  public static void main(String[] args){
    //set up scaner
    Scanner myScanner = new Scanner( System.in );
    //declare relevant variables
    int rows = 0;
    //block to check whether the user has entered a valid type
    System.out.println("Enter an integer from 1-10: ");
    //checks if input
    while (myScanner.hasNext()) {
      //checks if it is the proper type
      if (myScanner.hasNextInt()){
        rows = myScanner.nextInt();
        while ((rows < 1) || (rows > 10)){
          System.out.println("Error please enter an integer from 1 to 10");
          rows = myScanner.nextInt();
        }
        System.out.println("Entered");
        break;
      }
      //prints error and allows repeat of loop
      else {
        System.out.println("Error please enter an integer from 1 to 10");
        myScanner.next();
      } 
    }
    
    //Nested for loops to print pattern
    
    int i, j;
    for (i = rows; i >= 1; i--){
      for (j = 1; j <= i-1; j++){
        System.out.print(j + " ");
      }
      System.out.println(i);
    }
    
  }
}