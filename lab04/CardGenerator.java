//Bryce Cavey
//CSE 2
// 9/13/18
//Lab 2


public class CardGenerator{
  public static void main(String[] args){
 
     int card = (int)(Math.random()*52+1);
    
     String suit = "";
     String identity = "";
    
    if (card >= 1 && card <= 13) {
      suit = "Diamonds";
    } 
    if (card >= 14 && card <= 26) {
      suit = "Clubs";
    }
    if (card >= 27 && card <= 39) {
      suit = "Hearts";
    }
    if (card >= 40 && card <= 52) {
      suit = "Spades";
    }
    
    switch(card%13) {
      case 1: identity = "Ace";
        break;
      case 11: identity = "Jack";
        break;
      case 12: identity = "Queen";
        break;
      case 13: identity = "King";
        break;
      default:
        card = card%13;
        identity = String.valueOf(card);
        break;
    }
    
    System.out.println("You picked the " + identity + " of " + suit);
    
  }
}