 //Bryce Cavey
 //CSE 2
 // 10/18/18
 //Homework 6
 
 import java.util.Scanner;
 
 public class Sierpinski{
   
   //************************************************//
   // Method  to check if the hole is filled or not  //
   //************************************************//
   
   public static int isFilled(int x, int y, int u) {
     int filled = 1;
     //For loop to check whether the coordinate is filled at every generation or not
     for(int q = 0; q < u; q++){
       //CHecks if the coordinate is a center at any level
       if(( ((int)(x/Math.pow(3,q)) % 3) == 1) && ( ((int)(y/Math.pow(3,q)) % 3) == 1)){
         filled = 0;
         break;
       }
     }
     return filled;
    }
   
   public static void main(String[] args){
     
     //************************************************//
     //Inputs, outputs, and global variable declaration//
     //************************************************//
     
     //set up scaner
     Scanner myScanner = new Scanner( System.in );
     
     //Declare a few relevant  integer variables
     int g = 0;
     int x = 0;
     int y = 0;
     
     
     //Input the ASCII Character from the user and store it to a char
     System.out.println("Please enter an ASCII character to use for the Sierpinski carpet: ");
     String temp = myScanner.next();
 	   char result = temp.charAt(0);
     
     System.out.println("Enter the number of generations for the Sierpinski carpet to generate, from 0 to 5:");
     while (myScanner.hasNext()) {
       //checks if it is the proper type
       if (myScanner.hasNextInt()){
         g = myScanner.nextInt();
         while (g < 0 || g > 5 ){
           System.out.println("Error: Please enter a number from 0 5 inclusive.");
           g = myScanner.nextInt();
         }
         break;
       }
       //prints error and allows repeat of loop
       else {
         System.out.println("Error please enter an integer between 0 and 5");
         myScanner.next();
       } 
     }
     
     //****************************************//
     //Logic to generate the sierpinski carpet.// 
     //****************************************//
     
     //Determine the dimensions of the carpet
     x = (int)Math.pow(3,g);
     y = (int)Math.pow(3,g);
     
     System.out.println("The width is :" + x);
     System.out.println("The height is :" + y);
     
     //Outer loop to go up and down rows when outputting
     for (int i = 0; i < y; i++ ){
       //inner loop to go side to side of the carpet as generating
       for (int q = 0; q < x; q++ ){
         //if hole is filled then put the character, if not put two spaces
         if (isFilled(i, q, g) == 1){
           System.out.print(result + " ");
         }
         //if its empty just print spaces
         else{
           System.out.print("  ");
         }
         
       }
       System.out.println(" ");
     }
   }
 }