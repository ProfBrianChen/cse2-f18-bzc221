//Bryce Cavey
//CSE 2
// 10/25/18
//Lab 7

import java.util.Scanner;
import java.util.Random;

public class lab07{
  //Generates a random past tense verb
  public static String randomAdj(){
    //generate random number and select from a switch statement
    
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    
    String result = " ";
    
    switch(randomInt){
      
      case 0: result = "big";
              break;
      case 1: result = "huge";
            break;
      case 2: result = "yoked";
            break;
      case 3: result = "small";
            break;
      case 4: result = "tiny";
            break;
      case 5: result = "fast";
            break;
      case 6: result = "brown";
            break;
      case 7: result = "slow";
            break;
      case 8: result = "speedy";
            break;
      case 9: result = "round";
            break;
  
    }
    return result;
  }
  //Generates a random past tense verb
  public static String randomNpn(){
    //generate random number and select from a switch statement

    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    
    String result = " ";
    
    switch(randomInt){
      
      case 0: result = "bridge";
              break;
      case 1: result = "car";
            break;
      case 2: result = "bus";
            break;
      case 3: result = "generator";
            break;
      case 4: result = "wheels";
            break;
      case 5: result = "motor";
            break;
      case 6: result = "brother";
            break;
      case 7: result = "mother";
            break;
      case 8: result = "television";
            break;
      case 9: result = "computer";
            break;
  
    }
    return result;
  }
  //Generates a random past tense verb
  public static String randomPtv(){
    //generate random number and select from a switch statement

    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    
    String result = " ";
    
    switch(randomInt){
      
      case 0: result = "ran";
              break;
      case 1: result = "spun";
            break;
      case 2: result = "drove";
            break;
      case 3: result = "flew";
            break;
      case 4: result = "threw";
            break;
      case 5: result = "analyzed";
            break;
      case 6: result = "discovered";
            break;
      case 7: result = "walked";
            break;
      case 8: result = "sang";
            break;
      case 9: result = "drank";
            break;
  
    }
    return result;
  }
  
   //Generates a random past tense verb
  public static String randomObj(){
    //generate random number and select from a switch statement

    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    
    String result = " ";
    
    switch(randomInt){
      
      case 0: result = "book";
              break;
      case 1: result = "floor";
            break;
      case 2: result = "bottle";
            break;
      case 3: result = "barbell";
            break;
      case 4: result = "gun";
            break;
      case 5: result = "controller";
            break;
      case 6: result = "mouse";
            break;
      case 7: result = "glasses";
            break;
      case 8: result = "desk";
            break;
      case 9: result = "bed";
            break;
  
    }
    return result;
  }
  
  public static String thesis(){
    //Prints out the sentence using methods to randomly generate the words when needed
         //Makes a variable that can be passed for the subject
         String subject = " ";
         subject = randomObj();
    
         System.out.print("The "); 
         System.out.print(randomAdj() + " ");
         System.out.print(randomAdj() + " ");
         System.out.print(subject + " ");
         System.out.print(randomPtv() + " ");
         System.out.print("the ");
         System.out.print(randomAdj() + " ");
         System.out.println(randomObj() + ".");
    
         return subject;
  }
  
    public static String second(String subject){
    //Prints out the sentence using methods to randomly generate the words when needed
         System.out.print("The " + subject + " ");
         System.out.println(randomAdj() + " " + randomAdj() + " to " + randomPtv() + " " + randomObj() + ".");
         return subject;
    }
    
    public static String conclude(String subject){
    //Prints out the sentence using methods to randomly generate the words when needed
         System.out.print("That " + subject + " ");
         System.out.println(randomPtv() + " her " + randomObj() + ".");
         return subject;
    }
  //generate random number and select from a switch statement
  
  public static void main(String[] args){
      Scanner myScanner = new Scanner( System.in );
      
      int i = 1;
      
      while(i==1){
        //Take the subject 
        String subject = thesis();
        
        //Print second sentence
        subject = second(subject);
        
        //print conclusion
        subject = conclude(subject);
        
        //Check with user if they wat to generate anotehr sentence
        System.out.println("Enter 1 for another sentence, enter 0 to stop generating: ");
        i = myScanner.nextInt();
     
      }
    
  }
}