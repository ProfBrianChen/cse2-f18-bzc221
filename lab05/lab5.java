//Bryce Cavey
//CSE 2
// 10/4/18
//Lab 5

import java.util.Scanner;

public class lab5{
  public static void main(String[] args){
    //set up scaner
    Scanner myScanner = new Scanner( System.in );
    //declare relevant variables
    int courseNum = 0;
    String department = " ";
    int courseMeet = 0;
    int hour = 0;
    int minutes = 0;
    String instructor = " ";
    int size = 0;
    
    //block to check whether the user has entered a valid type
    System.out.println("Enter a course number: ");
    //checks if input
    while (myScanner.hasNext()) {
      //checks if it is the proper type
      if (myScanner.hasNextInt()){
        courseNum = myScanner.nextInt();
        break;
      }
      //prints error and allows repeat of loop
      else {
        System.out.println("Error please enter an integer");
        myScanner.next();
      } 
    }
    
    //block to check whether the user has entered a valid type
    System.out.println("Enter the department name: ");
    while (myScanner.hasNext()) {
      //checks if it is the proper type
      if (!myScanner.hasNextDouble() && !myScanner.hasNextInt()){
        department = myScanner.next();
        break;
      }
      //prints error and allows repeat of loop
      else {
        System.out.println("Error please enter a string");
        myScanner.next();
      } 
    }
    
    //block to check whether the user has entered a valid type
    System.out.println("Enter number of times course meets per week");
    while (myScanner.hasNext()) {
      //checks if it is the proper type
      if (myScanner.hasNextInt()){
        courseMeet = myScanner.nextInt();
        break;
      }
      //prints error and allows repeat of loop
      else {
        System.out.println("Error please enter an integer");
        myScanner.next();
      } 
    }
    
    //block to check whether the user has entered a valid type
    System.out.println("Enter the hour the course meets");
    while (myScanner.hasNext()) {
      //checks if it is the proper type
      if (myScanner.hasNextInt()){
        hour = myScanner.nextInt();
        break;
      }
      //prints error and allows repeat of loop
      else {
        System.out.println("Error please enter an integer");
        myScanner.next();
      } 
    }
    //block to check whether the user has entered a valid type
    System.out.println("Enter the minute time the course meets");
    while (myScanner.hasNext()) {
      //checks if it is the proper type
      if (myScanner.hasNextInt()){
        minutes = myScanner.nextInt();
        break;
      }
      //prints error and allows repeat of loop
      else {
        System.out.println("Error please enter an integer");
        myScanner.next();
      } 
    }
    
    //block to check whether the user has entered a valid type
    System.out.println("Enter the instructor name: ");
    while (myScanner.hasNext()) {
      //checks if it is the proper type
      if (!myScanner.hasNextDouble() && !myScanner.hasNextInt()){
        instructor = myScanner.next();
        break;
      }
      //prints error and allows repeat of loop
      else {
        System.out.println("Error please enter a string");
        myScanner.next();
      } 
    }
    
    //block to check whether the user has entered a valid type
    System.out.println("Enter the class size");
    while (myScanner.hasNext()) {
      //checks if it is the proper type
      if (myScanner.hasNextInt()){
        size = myScanner.nextInt();
        break;
      }
      //prints error and allows repeat of loop
      else {
        System.out.println("Error please enter an integer");
        myScanner.next();
      } 
    }
    
    //Block of output
    System.out.println("The course number is: " + courseNum);
    System.out.println("The department is: " + department);
    System.out.println("The course meets this many times a week: " + courseMeet);
    System.out.println("The course meets at " + hour + ":" + minutes);
    System.out.println("The instructor name is: " + instructor);
    System.out.println("The number of students in the class is: " + size);

    
  }
}