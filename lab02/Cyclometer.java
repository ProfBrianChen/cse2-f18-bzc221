//Bryce Cavey
//9/6/2018
//CSE 2018
//This program will record two kinds of data, the time elapsed in seconds, and the number of rotations of the front wheel during
// that time. For two trips it will output the number of minutes for each trip, the distance, the counts for each trip, and distance for two trips conmbined

public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
      
        //Input data
        int secsTrip1=480;  //Seconds for trip 1
       	int secsTrip2=3220;  //Seconds trip 2 took
    		int countsTrip1=1561;  //Counts for how many times trip 1 was done
		    int countsTrip2=9037; //Counts for how many times trip 2 was done
  
        //Useful variables for calculations
        double wheelDiameter=27.0,  //Variable to describe wheel diameter in inches
      	PI=3.14159, // Value of pi for calculations
  	    feetPerMile=5280,  // Constant number of feet per mile
  	    inchesPerFoot=12,   // Number of inches per foot for calculatios
  	    secondsPerMinute=60;  //Number of seconds in a minute for calculations 
	      double distanceTrip1, distanceTrip2, totalDistance;  // Doubles to store the distance we calculate for our trip

        //Print out the number of minutes and counts after conversions
        System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts."); //Prints out minutes and counts for trip 1 after the calculation for minutes per trip
	      System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts."); //Prints out minutes and counts for trip 1 after the calculation for minutes per trip
     
        //Calculation for the distance trip 1 took based on wheel rotations, wheel diameter, and PI.
       	distanceTrip1=countsTrip1*wheelDiameter*PI;
      	// Above gives distance in inches
    	  //(for each count, a rotation of the wheel travels
    	  //the diameter in inches times PI)
        //Calculation to find the distance in miles rather than feet.
	      distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	      distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
	      totalDistance=distanceTrip1+distanceTrip2;
      
        //Print out the output data.
        //This prints out the data in a well formatted and easy to understand segment
        System.out.println("Trip 1 was "+distanceTrip1+" miles");
	      System.out.println("Trip 2 was "+distanceTrip2+" miles");
	      System.out.println("The total distance was "+totalDistance+" miles");


	}  //end of main method   
} //end of class
