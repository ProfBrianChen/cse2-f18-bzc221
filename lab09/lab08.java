//Bryce Cavey
//CSE 2
// 10/26/18
//HW 7

public class lab08{
  public static int[] copy(int[] arr){
    int[] output = new int[arr.length];
    for(int k = 0; k<arr.length; k++){
      output[k] = arr[k];
    }
    return output;
  }
  
  public static void inverter(int[] array0){
    int temp = 0;
    for(int k = array0.length-1; k>=4; k--){
      temp = array0[k];
      array0[k] = array0[array0.length-k-1];
      array0[array0.length-k-1] = temp;
    }
    return;
  }
  
  public static int[] inverter2(int[] arr){
    int[] output = copy(arr);
    for(int k = arr.length-1; k > -1; k--){
      output[arr.length-k-1] = arr[k];
    }
    return output;
  }
  
  public static void print(int[] list){
    //for loop to cycle through and print the lists out
    System.out.println("");
    System.out.println("Printing output");
    for(int i = 0; i < list.length; i++){
      System.out.print(list[i] + " ");
    }
    return;
  }
  public static void main(String args[]){
    int[] array0 = {1, 2, 3, 4, 5, 6, 7, 8};
    int[] array1 = copy(array0);
    int[] array2 = copy(array0);
    
    //Pass and print array0
    inverter(array0);
    print(array0);
    
    //Pass and print array1
    inverter2(array1);
    print(array1);
    
    //pass declare and print array3
    int[] array3 = inverter2(array2);
    print(array3);
    
  }
}
