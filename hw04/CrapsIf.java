//Bryce Cavey
//CSE 2
// 9/20/18
// HW 4

import java.util.Scanner;

public class CrapsIf{
  public static void main(String[] args){
    //Set up scanner
    Scanner myScanner = new Scanner(System.in);
    //Ask user 
    System.out.println("Would you like to enter the value of the dice or randomly generate it? Enter 1 for enter and 2 for random");
    //Create a variable for the inputted response and the dies
    int choice = myScanner.nextInt();
    int die1 = 0;
    int die2 = 0;
    
    //declare a string for slang
    String slang = "";
    
    //Big if statement to determine whether the user wants to input or whether they want to randomly generate  
    if (choice == 1){
      //Prompt the user and input the value of die 1
      System.out.println("Enter the rolled value of die 1: ");
      die1 = myScanner.nextInt();
      //Check to make sure a valid number was entered
      if (die1 > 6 || die1 < 0){
        System.out.println("Please enter a number between 1 and 6");
        die1 = myScanner.nextInt();
      }
      //Prompt the user and input the value of die 2
      System.out.println("Enter the rolled value of die 2: ");
      die2 = myScanner.nextInt();
      //Check to make sure a valid number was entered
      if (die2 > 6 || die2 < 0){
        System.out.println("Please enter a number between 1 and 6");
        die2 = myScanner.nextInt();
      } 
    }
    if (choice == 2){
      //Randomly generate and declare die1 and die2 using math rand
      die1 = (int)(Math.random()*6)+1;
      die2 = (int)(Math.random()*6)+1;
      
      
    }
    
    //Big block of if statements for all the possible outcomes
    //Determines the outcome and assigns the slang term
      if (die1 == 1 && die2 == 1){
        slang = "snake eyes";
      }
      if ((die1 == 1 && die2 == 2) || (die2 == 1 && die1 == 2)){
        slang = "ace deuce";
      }
      if ((die1 == 1 && die2 == 3) || (die2== 1 && die1 == 3)){
        slang = "easy four";
      }
      if ((die1 == 1 && die2 == 4) || (die2 == 1 && die1 == 4)){
        slang = "fever five";
      }
      if ((die1 == 1 && die2 == 5) || (die2 == 1 && die1 == 5)){
        slang = "easy six";
      }
      if ((die1 == 1 && die2 == 6) || (die2 == 1 && die1 == 6)){
        slang = "seven out";
      }
      if (die1 == 2 && die2 == 2){
        slang = "hard four";
      }
      if ((die1 == 2 && die2 == 3) || (die2 == 2 && die1 == 3)){
        slang = "fever five";
      }
      if ((die1 == 2 && die2 == 4) || (die2 == 2 && die1 == 4)){
        slang = "easy six";
      }
      if ((die1 == 2 && die2 == 5) || (die2 == 2 && die1 == 5)){
        slang = "seven out";
      }
      if ((die1 == 2 && die2 == 6) || (die2 == 2 && die1 == 6)){
        slang = "easy eight";
      }
      if (die1 == 3 && die2 == 3){
        slang = "hard six";
      }
      if ((die1 == 3 && die2 == 4) || (die2 == 3 && die1 == 4)){
        slang = "seven out";
      }
      if ((die1 == 3 && die2 == 5) || (die2 == 3 && die1 == 5)){
        slang = "easy eight";
      }
      if ((die1 == 3 && die2 == 6) || (die2 == 3 && die1 == 6)){
        slang = "nine";
      }
      if (die1 == 4 && die2 == 4){
        slang = "hard eight";
      }
      if ((die1 == 4 && die2 == 5) || (die2 == 4 && die1 == 5)){
        slang = "nine";
      }
      if ((die1 == 4 && die2 == 6) || (die2 == 4 && die1 == 6)){
        slang = "easy ten";
      }
      if (die1 == 5 && die2 == 5){
        slang = "hard ten";
      }
      if ((die1 == 5 && die2 == 6) || (die2 == 5 && die1 == 6)){
        slang = "yo-leven";
      }
      if (die1 == 6 && die2 == 6){
        slang = "boxcars";
      }
    
      //Tell the user the generated or selected dies
      System.out.println("Die one is: " + die1 + " and die two is: " + die2);
      //Print out the result of the dice roll
      System.out.println("You rolled: " + slang);
  }
}