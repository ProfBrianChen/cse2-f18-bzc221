//Bryce Cavey
//CSE 2
// 9/20/18
// HW 4

import java.util.Scanner;

public class CrapsSwitch{
  public static void main(String[] args){
    
    //Set up scanner
    Scanner myScanner = new Scanner(System.in);
    //Ask user 
    System.out.println("Would you like to enter the value of the dice or randomly generate it? Enter 1 for enter and 2 for random");
    //Create a variable for the inputted response and the dies
    int choice = myScanner.nextInt();
    int die1 = 0;
    int die2 = 0;
    
    //declare a string for slang
    String slang = "";
    
    //Big switch statement to determine whether the user wants to input or whether they want to randomly generate  
    switch (choice) {
      case 1: 
          //Prompt the user and input the value of die 1
          System.out.println("Enter the rolled value of die 1: ");
          die1 = myScanner.nextInt();
          //Check to make sure a valid number was entered
          switch (die1) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
              break;
            default:  System.out.println("Please enter a number between 1 and 6");
              die1 = myScanner.nextInt();
              break;
          }
          //Prompt the user and input the value of die 2
          System.out.println("Enter the rolled value of die 2: ");
          die2 = myScanner.nextInt();
          //Check to make sure a valid number was entered
          switch (die2) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
              break;
            default:  System.out.println("Please enter a number between 1 and 6");
              die2 = myScanner.nextInt();
              break;
          }
          break;
      case 2:
          //Randomly generate and declare die1 and die2 using math rand
          die1 = (int)(Math.random()*6)+1;
          die2 = (int)(Math.random()*6)+1;
          break;
      }
    
    
    //Big block of switch statements for all the possible outcomes
    //Determines the value of first and second die and then assigns proper slang
    switch (die1) {
      //Die 1 is 1 
      case 1:
        switch (die2){
          case 1:
            slang = "snake eyes";
            break;
          case 2:
            slang = "ace deuce";
            break;
          case 3:
            slang = "easy four";
            break;
          case 4:
            slang = "fever five";
            break;
          case 5:
            slang = "easy six";
            break;
          case 6:
            slang = "seven out";
            break;
        }
        break;
    //Die 1 is 2
    case 2:
        switch (die2){
          case 1:
            slang = "ace deuce";
            break;
          case 2:
            slang = "hard four";
            break;
          case 3:
            slang = "fever five";
            break;
          case 4:
            slang = "easy six";
            break;
          case 5:
            slang = "seven out";
            break;
          case 6:
            slang = "easy eight";
            break;
        }
        break;
    //Die 1 is 3
    case 3:
        switch (die2){
          case 1:
            slang = "easy four";
            break;
          case 2:
            slang = "fever five";
            break;
          case 3:
            slang = "hard six";
            break;
          case 4:
            slang = "seven out";
            break;
          case 5:
            slang = "easy eight";
            break;
          case 6:
            slang = "nine";
            break;
        }
        break;
    //Die 1 is 4
    case 4:
        switch (die2){
          case 1:
            slang = "fever five";
            break;
          case 2:
            slang = "easy six";
            break;
          case 3:
            slang = "seven out";
            break;
          case 4:
            slang = "hard eight";
            break;
          case 5:
            slang = "nine";
            break;
          case 6:
            slang = "easy ten";
            break;
        }
        break;
    //Die 1 is 5
    case 5:
        switch (die2){
          case 1:
            slang = "easy six";
            break;
          case 2:
            slang = "seven out";
            break;
          case 3:
            slang = "easy eight";
            break;
          case 4:
            slang = "nine";
            break;
          case 5:
            slang = "hard ten";
            break;
          case 6:
            slang = "yo-leven";
            break;
        }
        break;
     //Die 1 is 6
     case 6:
        switch (die2){
          case 1:
            slang = "seven out";
            break;
          case 2:
            slang = "easy eight";
            break;
          case 3:
            slang = "nine";
            break;
          case 4:
            slang = "easy ten";
            break;
          case 5:
            slang = "yo-leven";
            break;
          case 6:
            slang = "boxcars";
            break;
        }
        break;
    }
      //Tell the user the generated or selected dies
      System.out.println("Die one is: " + die1 + " and die two is: " + die2);
      //Print out the result of the dice roll
      System.out.println("You rolled: " + slang);
  }
}