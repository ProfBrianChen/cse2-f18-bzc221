//Bryce Cavey
//9/6/2018
//CSE 2018
//HW 2018
//This program will compute the cost of the items bought including the PA state 6% sales tax. 

public class Arithmetic {
    	// main method required for every Java program
   	public static void main(String[] args) {
      
      //The following are input varibales
      //Number of pairs of pants
      int numPants = 3;
      
      //Cost per pair of pants
      double pantsPrice = 34.98;

      //Number of sweatshirts
      int numShirts = 2;
      //Cost per shirt
      double shirtPrice = 24.99;

      //Number of belts
      int numBelts = 1;
      //cost per belt
      double beltCost = 33.99;

      //the tax rate
      double paSalesTax = 0.06;
      
      //The following are variables which will store calculation results
      //Variable declaration for the total price of pants.
      double totalPants;
      //Variable declaration for the total price of shirts.
      double totalShirts;
      //Variable declaration for the total price of belts.
      double totalBelts;
      //Variable declaration for the total tax on pants. 
      double totalPantsTax;
      //Variable declaration for the total tax on shirts.
      double totalShirtsTax;
      //Variable declaration for the total tax on belts.
      double totalBeltsTax;
      //Variable declaration for total cost of purchases before tax 
      double totalCost;
      //Variable declaration for total sales tax to be collected
      double totalTax;
      //Variable declaration for the total amount due
      double totalDue;
      
      //Calculation for total cost of all pants
      totalPants = (int)(pantsPrice*numPants*100)/100.0;
      //Calculation for total cost of all shirts
      totalShirts = (int)(shirtPrice*numShirts*100)/100.0;
      //Calculation for total cost of all belts
      totalBelts = (int)(beltCost*numBelts*100)/100.0;
      //Calculation for the tax on pants
      totalPantsTax = (int)(totalPants*0.06*100)/100.0;
      //Calculation for the tax on shirts
      totalShirtsTax = (int)(totalShirts*0.06*100)/100.0;
      //Calculation for the tax on belts
      totalBeltsTax = (int)(totalBelts*0.06*100)/100.0;
      //Calculation for the total cost before tax
      totalCost = totalPants + totalShirts + totalBelts;
      //Calculation for the total tax to be collected
      totalTax = totalPantsTax + totalShirtsTax + totalBeltsTax;
      //Calculation for the total to be charged
      totalDue = totalCost + totalTax;
      
      System.out.println("The total cost of all the pants is $" + totalPants + " and the tax to be charged for the pants is $" + totalPantsTax);
      System.out.println("The total cost of all the shirts is $" + totalShirts + " and the tax to be charged for the shirts is $" + totalShirtsTax);
      System.out.println("The total cost of all the belts is $" + totalBelts + " and the tax to be charged for the belts is $" + totalBeltsTax);
      System.out.println("The total cost of all of the purchases before tax is $" + totalCost + " and the total tax on this is $" + totalTax);
      System.out.println("The total amount due including tax for all purchases is: $" + totalDue);
    }
}