//Bryce Cavey
//CSE 2
// 10/26/18
//HW 7

import java.util.Scanner;

public class hw07{
  
  public static String sampleText(){
    //Set up scanner for input
    Scanner myScanner = new Scanner( System.in );
    //Input the users input to a string called input
    String input = " ";
    System.out.println("Enter a sample text: ");
    input = myScanner.nextLine();
    
    //Go to new line for nice output and show users input
    System.out.println(" ");
    System.out.println(" ");
    System.out.print("You entered: ");
    System.out.println(input);
    System.out.println(" ");
    System.out.println(" ");
    
    return input;
  }
  
  public static String printMenu(){
    //Set up scaner within this method
    Scanner myScanner = new Scanner( System.in );
    //Declare a string for the users choice
    String choice = " "; 
    //Print out menu
    System.out.println("MENU");
    System.out.println(" ");
    System.out.println("c - Number of non-whitespace characters");
    System.out.println("w - Number of words");
    System.out.println("f - Find text");
    System.out.println("r - Replace all !'s");
    System.out.println("s - Shorten spaces");
    System.out.println("q - Quit");
    System.out.println(" ");
    System.out.println("Choose an option: ");
    
    //Input users choice
    choice = myScanner.next();
    //return the choice of the user
    return choice;
  }
  
  //Method to get num of non whitespace characters
  public static int getNumOfNonWSCharacters(String input){
    //Declare some counters and find length of input
    int n = input.length();
    int count = 0;
    int i = 0;
    //While loop to scan nicely through loop to count non whitespace char
    while ( i < n){
      if (input.charAt(i) != ' ') {
        count++; 
      }
      i++;
    }
    
    //Return the total count
    return count;
  }
  
  //Method to get num of words
  public static int getNumOfWords(String input){
    //Declare some counters and find length of input
    int n = input.length();
    int count = 0;
    int i = 0;
    //While loop to scan nicely through loop to count words
    while ( i < n){
      if (input.charAt(i) == ' ' && input.charAt(i-1) != ' ') {
        count++; 
      }
      i++;
    }
    
    //Add to count since we always miss last words unless the user put a space after last words
    if (input.charAt(n-1) != ' '){
      count++;
    }
   
    //Return the total count
    return count;
  }
  
  //Method to find text
  public static int findText(String input, String search){
    //Declare some counters and find length of input
    int n1 = input.length();
    int n2 = search.length();
    
    int count = 0;
    int found = 0;
    int i = 0;
    int q = 0;
    
    //While loop to scan nicely through loop to count words
    while (i < n1){
      //While loop that cycles through to see if there is a word match following any character
      while (q < n2){
        //Check to make sure we are still checking in bounds, i.e. not the last char. 
        if ((i + q) < n1){
          //Check to see if the letters following a match are the rest of a word we want.
          if (input.charAt(i + q) == search.charAt(q)) {
          count++; 
          } 
        }
        
        q++;
      }
      q = 0;
      //If all the proper following is found then we have a match!
      if (count == n2) {
        found++; 
      }
      count = 0;
      i++;
    }
    
    return found;
  }
  
  //Method to replaceExclamation
  public static String replaceExclamation(String input){
    //Replace any ! with .
    input = input.replace("!", ".");
    
    return input;
  }
  
  //Method to shortenSpace
  public static String shortenSpace(String input){
    //Replace any double space with a single
    //Loop through the length of the string to eliminate any double spaces with multiple passes
    for (int i = 0; i< input.length(); i++){
      input = input.replaceAll("  ", " ");
    }
    return input;
  }
  
  
  
  
  //Main class
  public static void main(String args[]){
    //Set up scanner for input
    Scanner myScanner = new Scanner( System.in );
    //Create a variable to hold the users input and their menu choice.
    String input = " ";
    String choice = " ";
    //Gather user input from method
    input = sampleText();
    
    //Print out the menu and gather the users choice. Keep doing this until choice is equal to a valid choice. Do while so it runs at least once
    do {
      choice = printMenu();
    //(!((choice.equals("c")) || (choice.equals("w")) || (choice.equals("f")) || (choice.equals("r")) || (choice.equals("s")) || (choice.equals("q"))) && 
      if(choice.equals("q")){
        break;
      }
    //Switch statement to decide which string operation method to go into
      switch (choice){
        case "c":
          System.out.println("The number of non-whitespace characters: " + getNumOfNonWSCharacters(input));
          System.out.println(" ");
          break;
        case "w":
          System.out.println("The number of words: " + getNumOfWords(input));
          getNumOfWords(input);
          System.out.println(" ");
          break;
        case "f":
          System.out.println("Enter a word or phrase to be found:");
          String search = myScanner.nextLine();
          System.out.println("\"" + search + "\" instances: " + findText(input, search));
          System.out.println(" ");
          break;
        case "r":
          System.out.println("Edited text: " + replaceExclamation(input));
          System.out.println(" ");
          break;
        case "s":
          System.out.println("Edited text: " + shortenSpace(input));
          System.out.println(" ");
          break;
      }
    } while(choice != "q");
  }
}