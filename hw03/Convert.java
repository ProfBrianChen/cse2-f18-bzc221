//Bryce Cavey
//CSE 2
// 9/13/18
// HW 3

import java.util.Scanner;

public class Convert{
  public static void main(String[] args){
    
    //Set up scanner
    Scanner myScanner = new Scanner(System.in);
    //Prompt the user to enter a double for # of acres of land affected by hurricane
    System.out.print("Enter the number of acres of land affected by the hurricane as a double: ");
    //Input the acres as double
    double acres = myScanner.nextDouble();
    //Prompt the user to enter a double for # of acres of land affected by hurricane
    System.out.print("Enter the rainfall of the affected area in inches: ");
    //Input the acres as double
    double rainfall = myScanner.nextDouble();
    
    //compute rain in cubic miles
    double rainVol = (0.0015625*acres)*rainfall/(12*5280);
    
    //print out the volume of rain in cubic miles
    System.out.println("The volume of rain that fell on the area in cubic miles is: " + rainVol );
    
  }
}