//Bryce Cavey
//CSE 2
// 9/13/18
// HW 3

import java.util.Scanner;

public class Pyramid{
  public static void main(String[] args){
    
    //Set up scanner
    Scanner myScanner = new Scanner(System.in);
    
    //Prompt user for the dimensions of the square side of the pyramid
    System.out.print("Enter the length of a side on the square side of the pyramid: ");
    //input the length of the side
    double squareLength = myScanner.nextDouble();
    //Prompt user for the height of the pyramid
    System.out.print("Enter the height of the pyramid: ");
    //Input the height of the pyramid
    double height = myScanner.nextDouble();
    
    //calculate the volume of the pyramid
    double volume = ((Math.pow(squareLength, 2)) * height) / 3;
    //Output the volume of the pyramid
    System.out.println("The volume of the pyramid is " + volume + " cubed units");
    
  }
}
    