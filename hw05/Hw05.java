//Bryce Cavey
//CSE 2
// 10/4/18
//Homework 5

import java.util.Scanner;

public class Hw05{
  public static void main(String[] args){
    //set up scaner
    Scanner myScanner = new Scanner( System.in );
    //declare the cards together and the number of loops. A ticker as well
    int loops = 0;
    int card1 = 0;
    int card2 = 0;
    int card3 = 0;
    int card4 = 0;
    int card5 = 0;
    int i = 0;
    //Declare the counters for each type of hand
    int foak = 0;
    int toak = 0;
    int tp = 0;
    int op = 0;
    int fh = 0;
    //Declare the counter for the similarity type and number
    int type1 = -1;
    int type2 = -1;
    int countType1 = 0;
    int countType2 = 0;
    
    //block to ask how many times to generate a hand and check to make sure input is a valid integer
    System.out.println("Enter the number of times to simulate a hand");
    //checks if input
    while (myScanner.hasNext()) {
      //checks if it is the proper type
      if (myScanner.hasNextInt()){
        loops = myScanner.nextInt();
        break;
      }
      //prints error and allows repeat of loop
      else {
        System.out.println("Error please enter an integer");
        myScanner.next();
      } 
    }
    
    //Generate the hand of cards 
    while(i < loops){
      //While loop to define all the cards. Checks to make sure the card isnt equal to any others, and also makes sure it isnt assigned already
      while (card1 == 0 || ((card1 == card2) || (card1 == card3) || (card1 == card4) || (card1 == card5))) {
        card1 = (int)(Math.random()*52+1);
      }
      while (card2 == 0 || ((card2 == card1) || (card2 == card3) || (card2 == card4) || (card2 == card5))) {
        card2 = (int)(Math.random()*52+1);
      }
      while (card3 == 0 || ((card3 == card1) || (card3 == card2) || (card3 == card4) || (card3 == card5))) {
        card3 = (int)(Math.random()*52+1);
      }
      while (card4 == 0 || ((card4 == card1) || (card4 == card2) || (card4 == card3) || (card4 == card5))) {
        card4 = (int)(Math.random()*52+1);
      }
      while (card5 == 0 || ((card5 == card1) || (card5 == card2) || (card5 == card3) || (card5 == card4))) {
        card5 = (int)(Math.random()*52+1);
      }
      //determine the face value of each of the cards
      int face1 = (card1%13);
      int face2 = (card2%13);
      int face3 = (card3%13);
      int face4 = (card4%13);
      int face5 = (card5%13);
      //Determine all similarities. Either 2 cases of similarities or just one are possible.
      //Find the first type of card that matches
      if (face1 == face2 || face1 == face3 || face1 == face4 || face1 == face5){
        type1 = face1;
      }
      if (face2 == face3 || face2 == face4 || face2 == face5){
        type1 = face2;
      }
      if (face3 == face4 || face3 == face5){
        type1 = face3;
      }
      if (face4 == face5){
        type1 = face4;
      }
      //Now count all that match within this type
      if (face1 == type1 && ((face1 == face2) || (face1 == face3) || (face1 == face4) || (face1 == face5))){
        countType1++;
      }
      if (face2 == type1 && ((face2 == face3) || (face2 == face4) || (face2 == face5))){
        countType1++;
      }
      if (face3 == type1 && ((face3 == face4) || (face3 == face5))){
        countType1++;
      }
      if (face4 == type1 && ((face4 == face5))){
        countType1++;
      }
      //Find the second type of card that matches. This time we need to ensure it doesnt match the first type.
      if ((face1 == face2 || face1 == face3 || face1 == face4 || face1 == face5) && (face1 != type1)){
        type2 = face1;
      }
      if (((face2 == face3 || face2 == face4 || face2 == face5) && face2 == type2) && (face2 != type1)){
        type2 = face2;
      }
      if (((face3 == face4 || face3 == face5) && face3 == type2) && (face3 != type1)){
        type2 = face3;
      }
      if (((face4 == face5) && face4 == type2) && (face4 != type1)){
        type2 = face4;
      }
      //Now count all that match within this type
      if (face1 == type2 && ((face1 == face2) || (face1 == face3) || (face1 == face4) || (face1 == face5))){
        countType2++;
      }
      if (face2 == type2 && ((face2 == face3) || (face2 == face4) || (face2 == face5))){
        countType2++;
      }
      if (face3 == type2 && ((face3 == face4) || (face3 == face5))){
        countType2++;
      }
      if (face4 == type2 && ((face4 == face5))){
        countType2++;
      }
      
      
      //Test for foak (There can only be one type and count must be incremented 3 times for 4 of a kind)
      if (countType1 == 3 || countType2 == 3){
        foak++;
      }
      //Test for toak (There must be three alike (count = 2) and the reamining 2 cards can not match so type 2 will still be -1)
      if ((countType1 == 2 || countType2 == 2) && (type2 == -1)){
        toak++;
      }
      //Test for tp (For two pair there must be exactly two pairs, each with a count of 1)
      if (type1 != -1 && type2 != -1){
        tp++;
      }
      //Test for op (type 2 must still be -1 and then count 1 will be 1 indicating a single pair)
      if ((countType1 == 1 && type2 == -1) || (countType2 == 1 && type1 == -1)){
        op++;
      }
      //Test for full house (count must be 2 for one type of card and the other type must just have just a count of 1)
      if ((countType1 == 2 && countType2 == 1) || (countType2 == 2 && countType1 == 1)){
        op++;
        toak++;
      }
     
      //Reset the card values for next loop
      card1 = card2 = card3 = card4 = card5 = 0;
      //Reset card type and type counter variables
      countType1 = countType2 = 0;
      type1 = -1; //-1 indicates no match
      type2 = -1; //-1 indicates no match
      //increment
      i++;
    }
 
    //Print out final output, while calculating the probability within the print statement
    System.out.println("The number of loops is: " + loops);
    System.out.printf("The probability of four-of-a-kind is: %.3f \n", ((double)foak/loops));
    System.out.printf("The probability of three-of-a-kind is: %.3f \n", ((double)toak/loops));
    System.out.printf("The probability of two-pair is: %.3f \n", ((double)tp/loops));
    System.out.printf("The probability of one-pair is: %.3f \n", ((double)op/loops));
  }
}